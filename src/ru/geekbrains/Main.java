package ru.geekbrains;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        //task1();
        //task2();
    }

    /**
     * Задача 1
     * Создать массив с набором слов (10-20 слов, должны встречаться повторяющиеся). Найти и вывести список
     * уникальных слов, из которых состоит массив (дубликаты не считаем). Посчитать сколько раз встречается
     * каждое слово.
     */
    public static void task1() {
        // Слова для создания массива с набором слов
        String[] words =  {"Апельсин", "Помело", "Гранат", "Каинито", "Личи", "Мелинжо", "Моква"};
        // Создаем массив с набором слов
        String[] arr = new String[20];
        for (int i = 0; i < arr.length; i++) {
            int wordIndex = (int) (Math.random() * words.length);
            arr[i] = words[wordIndex];
        }
        // Находим количество повторений
        HashMap<String, Integer> hm = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            Integer count = hm.get(arr[i]);
            hm.put(arr[i], count == null ? 1 : ++count);
        }
        // Создаем список уникальных слов
        ArrayList<String> unique = new ArrayList<>(hm.size());
        for (String key : hm.keySet()) {
            Integer count = hm.get(key);
            if (count == 1) unique.add(key);
        }
        // Выводим уникальные слова и количество повторений
        System.out.println("Массив с набором слов: \n" + Arrays.toString(arr));
        System.out.println("Список уникальных слов в массиве: \n" + unique);
        System.out.println("Количество повторений каждого слова: \n" + hm);
    }

    /**
     * Задача 2
     * Написать простой класс ТелефонныйСправочник, который хранит в себе список фамилий и телефонных номеров.
     * В этот телефонный справочник с помощью метода add() можно добавлять записи. С помощью метода get()
     * искать номер телефона по фамилии. Следует учесть, что под одной фамилией может быть несколько телефонов
     * (в случае однофамильцев), тогда при запросе такой фамилии должны выводиться все телефоны.
     */
    public static void task2() {
        PhoneBook phonebook = new PhoneBook();
        // Заполняем телефонную книгу
        String[] surnames = {"Антонов", "Петров", "Васильев", "Сидоренко", "Петренко", "Семенов"};
        for (int i = 0; i < 20; i++) {
            int number = 100000 + (int) (Math.random() * 900000);
            String name = surnames[(int) (Math.random() * surnames.length)];
            phonebook.add(name, number);
        }
        // Выводим телефонные номера для каждой фамилии
        for (int i = 0; i < surnames.length; i++) {
            System.out.println(surnames[i] + ":");
            System.out.println(phonebook.get(surnames[i]));
        }
    }
}
