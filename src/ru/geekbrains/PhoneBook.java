package ru.geekbrains;

import java.util.HashMap;
import java.util.HashSet;

public class PhoneBook {
    /**
     * Содержит записи телефонной книги.
     */
    private HashMap<String, HashSet<Integer>> items = new HashMap<>();

    /**
     * Добавление записи в телефонную книгу.
     * @param surname
     * @param number
     */
    public void add(String surname, int number) {
        HashSet<Integer> numbers;
        if (items.containsKey(surname)) numbers = items.get(surname);
        else numbers = new HashSet<>();
        numbers.add(number);
        items.put(surname, numbers);
    }

    /**
     * Получение списка номеров из телефонной книги по фамилии.
     *
     * @param surname
     * @return HashSet<Integer>|null
     */
    public HashSet<Integer> get(String surname) {
        return items.get(surname);
    }
}
